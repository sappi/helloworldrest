# Project Hello World Rest

## Function
A simple SpringBoot application who expose the following url http://localhost:8080/hello

As result you receive "Hello from HelloController"


## Maven

You can build the application with `./mvn clean install`
and run the application
```
./java -jar target/helloworldrest-0.0.1-SNAPSHOT.jar
```